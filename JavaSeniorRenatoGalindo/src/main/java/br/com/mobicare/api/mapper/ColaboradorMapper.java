package br.com.mobicare.api.mapper;

import java.util.ArrayList;
import java.util.List;

import br.com.mobicare.api.model.Colaborador;
import br.com.mobicare.api.vo.ColaboradorVO;

public class ColaboradorMapper {
	
	private Colaborador colaborador;
	
	private ColaboradorVO colaboradorVO;

	
	public ColaboradorMapper() {
		
		super();
	}

	
	public List<ColaboradorVO> getMapeamento ( List<Colaborador> colaboradores) {
		
		
		List colaboradoresVO = new ArrayList<>();
		
		for (Colaborador colaborador : colaboradores) {
			
			colaboradoresVO.add(getMapperColabordorVO(colaborador));
			
			
		}
		
		
		
		
		return colaboradoresVO;
		
		
	}
	
	
	public ColaboradorVO getMapperColabordorVO(Colaborador colaborador) {
		
		ColaboradorVO colaboradorVO = new ColaboradorVO();
		
		colaboradorVO.setNome(colaborador.getNome());
	    colaboradorVO.setEmail(colaborador.getEmail());
		
		
		return colaboradorVO;
	}
	
	
	
	public ColaboradorVO getMapperColabordorVO() {
		
		this.colaboradorVO.setNome(this.colaborador.getNome());
		this.colaboradorVO.setEmail(this.colaborador.getEmail());
		
		
		return colaboradorVO;
	}
	
	public ColaboradorMapper(Colaborador colaborador) {
		super();
		this.colaborador = colaborador;
	}

	public Colaborador getColaborador() {
		return colaborador;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}

	public ColaboradorVO getColaboradorVO() {
		return colaboradorVO;
	}

	public void setColaboradorVO(ColaboradorVO colaboradorVO) {
		this.colaboradorVO = colaboradorVO;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((colaborador == null) ? 0 : colaborador.hashCode());
		result = prime * result + ((colaboradorVO == null) ? 0 : colaboradorVO.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ColaboradorMapper other = (ColaboradorMapper) obj;
		if (colaborador == null) {
			if (other.colaborador != null)
				return false;
		} else if (!colaborador.equals(other.colaborador))
			return false;
		if (colaboradorVO == null) {
			if (other.colaboradorVO != null)
				return false;
		} else if (!colaboradorVO.equals(other.colaboradorVO))
			return false;
		return true;
	}
	
	
	

}
