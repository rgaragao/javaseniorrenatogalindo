package br.com.mobicare.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.mobicare.api.model.Setor;



public interface SetorRepository extends JpaRepository<Setor, Long> {

}
