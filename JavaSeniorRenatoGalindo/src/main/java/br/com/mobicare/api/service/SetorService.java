package br.com.mobicare.api.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;


import br.com.mobicare.api.model.Setor;

import br.com.mobicare.api.repository.SetorRepository;

@Service
public class SetorService {
	
	
	

	@Autowired
	private SetorRepository setorRepository;
	

	public Setor atualizar(Long codigo, Setor setor) {
		Setor setorSalva = buscarPessoaPeloCodigo(codigo);
		
		BeanUtils.copyProperties(setor, setorSalva, "codigo");
		return setorRepository.save(setorSalva);
	}


	
	public Setor buscarPessoaPeloCodigo(Long codigo) {
		Setor setorSalva = setorRepository.findOne(codigo);
		if (setorSalva == null) {
			throw new EmptyResultDataAccessException(1);
		}
		return setorSalva;
	}
	
	
	

	public Setor salvar(Setor setor) {
	
	
	
		return setorRepository.save(setor);
		

	}

}
