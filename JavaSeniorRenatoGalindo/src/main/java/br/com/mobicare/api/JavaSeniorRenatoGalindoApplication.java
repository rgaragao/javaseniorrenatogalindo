package br.com.mobicare.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaSeniorRenatoGalindoApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaSeniorRenatoGalindoApplication.class, args);
	}
}
