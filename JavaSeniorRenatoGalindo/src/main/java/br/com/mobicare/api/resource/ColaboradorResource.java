package br.com.mobicare.api.resource;


import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.mobicare.api.event.RecursoCriadoEvent;

import br.com.mobicare.api.model.Colaborador;
import br.com.mobicare.api.repository.ColaboradorRepository;

import br.com.mobicare.api.service.ColaboradorService;

import br.com.mobicare.api.vo.ColaboradorVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/colaboradores")
@Api(value="Colaboradores", description="Crud Responsavel pelo cadastramento de colaboradores")
public class ColaboradorResource {

	@Autowired
	private ColaboradorRepository colaboradorRepository;

	@Autowired
	private ColaboradorService colaboradorService;

	@Autowired
	private ApplicationEventPublisher publisher;
	
	@ApiOperation(value = "Listar colaboradores agrupados por setores",response = Iterable.class)
	@GetMapping("/setores/{codigo}")
	public List<ColaboradorVO> pesquisaSetor(@PathVariable Long codigo) {
		
						
		return colaboradorService.PesquisaSetores(codigo);
	}
	
	@ApiOperation(value = "Lista todos os colaboradores",response = Iterable.class)
	@GetMapping
	public List<Colaborador> pesquisar() {
		return colaboradorService.pesquisar();
	}
	@ApiOperation(value = "Lista Colaborador por codigo",response = Iterable.class)
	  @ApiResponses(value = {
	    	            @ApiResponse(code = 404, message = "O colaborador não foi encotrado.")
	    })
	@GetMapping("/{codigo}")
	public ResponseEntity<Colaborador> buscarPeloCodigo(@PathVariable Long codigo) {
		Colaborador colaborador = colaboradorService.buscarPessoaPeloCodigo(codigo);
		return colaborador != null ? ResponseEntity.ok(colaborador) : ResponseEntity.notFound().build();
	}

	@PostMapping()
	@ApiOperation(value = "Inserir Colaborador",response = Iterable.class)
	 		public ResponseEntity<Colaborador> criar(@Valid @RequestBody Colaborador colaborador,
			HttpServletResponse response) {

		Colaborador colaboradorSalvo = colaboradorService.salvar(colaborador);

		publisher.publishEvent(new RecursoCriadoEvent(this, response, colaboradorSalvo.getCodigo()));
		return ResponseEntity.status(HttpStatus.CREATED).body(colaboradorSalvo);
	}

	
	@ApiOperation(value = "Deletar Colaborador por codigo",response = Iterable.class)
	  @ApiResponses(value = {
	    	            @ApiResponse(code = 404, message = "O colaborador não foi encotrado.")
	    })
	@DeleteMapping("/{codigo}")
	@ResponseStatus(HttpStatus.OK)
	public void remover(@PathVariable Long codigo) {
		colaboradorRepository.delete(codigo);
	}
	
	
	@ApiOperation(value = "Alterar Colaborador por codigo",response = Iterable.class)
	  @ApiResponses(value = {
	    	            @ApiResponse(code = 404, message = "O colaborador não foi encotrado.")
	    })
	@PutMapping("/{codigo}")
	public ResponseEntity<Colaborador> atualizar(@PathVariable Long codigo,
			@Valid @RequestBody Colaborador colaborador) {
		try {
			Colaborador colaboradorSalvo = colaboradorService.atualizar(codigo, colaborador);
			return ResponseEntity.ok(colaboradorSalvo);
		} catch (IllegalArgumentException e) {
			return ResponseEntity.notFound().build();
		}
	}
}
