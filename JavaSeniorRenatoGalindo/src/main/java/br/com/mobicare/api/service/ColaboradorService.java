package br.com.mobicare.api.service;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import br.com.mobicare.api.mapper.ColaboradorMapper;
import br.com.mobicare.api.model.Colaborador;
import br.com.mobicare.api.repository.ColaboradorRepository;
import br.com.mobicare.api.repository.SetorRepository;
import br.com.mobicare.api.service.exception.SetorNaoInexistenteException;
import br.com.mobicare.api.vo.ColaboradorVO;


@Service
public class ColaboradorService {
	
		
		@Autowired
		private ColaboradorRepository colaboradorRepository;
		
		@Autowired
		private SetorRepository setorRepository;	
		
		public List<ColaboradorVO>  PesquisaSetores(Long codigo) {
			
			ColaboradorMapper colaboaradorMapper = new ColaboradorMapper();
			
			return colaboaradorMapper.getMapeamento(colaboradorRepository.findBySetorId(codigo));
		}
		
		
		public Colaborador salvar(Colaborador colaborador) {
		
	
			validaSetor(colaborador.getSetorId());
			
		
			return colaboradorRepository.save(colaborador);
			
	
		}
		
		public List<Colaborador> pesquisar(){
			return colaboradorRepository.findAll();
		}

		
		
		public Colaborador atualizar(Long codigo, Colaborador colaborador) {
		
			
			
			Colaborador colaboaradorSalva = buscarPessoaPeloCodigo(codigo);
			
			

			BeanUtils.copyProperties(colaborador, colaboaradorSalva, "codigo");
			
						
     		validaSetor(colaboaradorSalva.getSetorId());
			
			return colaboradorRepository.save(colaboaradorSalva);
		}

	
		
		public void validaSetor(Long codigo) {
			
			boolean setor = setorRepository.exists(codigo);
			
			if(setor == true){
				setor = setorRepository.exists(codigo);
			}else {
				
				throw new SetorNaoInexistenteException();
			}
			
			
			
			
		}
		
		public Colaborador buscarPessoaPeloCodigo(Long codigo) {
			Colaborador colaboradorSalva = colaboradorRepository.findOne(codigo);
	
			if (colaboradorSalva == null) {
				throw new EmptyResultDataAccessException(1);
			}
			return colaboradorSalva;
		}
		
		
		
		
	}


