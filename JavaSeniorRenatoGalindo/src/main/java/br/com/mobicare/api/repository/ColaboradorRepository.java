package br.com.mobicare.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.mobicare.api.model.Colaborador;

public interface ColaboradorRepository extends JpaRepository<Colaborador, Long>{

	
	
	public List <Colaborador> findBySetorId(Long setorId);
}
