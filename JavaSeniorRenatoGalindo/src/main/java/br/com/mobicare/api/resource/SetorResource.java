package br.com.mobicare.api.resource;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.mobicare.api.event.RecursoCriadoEvent;

import br.com.mobicare.api.model.Setor;

import br.com.mobicare.api.repository.SetorRepository;

import br.com.mobicare.api.service.SetorService;

@RestController
@RequestMapping("/setores")
public class SetorResource {

	@Autowired
	private SetorRepository setorRepository;

	@Autowired
	private SetorService setorService;

	@Autowired
	private ApplicationEventPublisher publisher;

	@GetMapping
	public List<Setor> pesquisar() {
		return setorRepository.findAll();
	}

	@GetMapping("/{codigo}")
	public ResponseEntity<Setor> buscarPeloCodigo(@PathVariable Long codigo) {
		Setor setor = setorService.buscarPessoaPeloCodigo(codigo);
		return setor != null ? ResponseEntity.ok(setor) : ResponseEntity.notFound().build();
	}

	@PostMapping
	public ResponseEntity<Setor> criar(@Valid @RequestBody Setor setor, HttpServletResponse response) {
		Setor setorSalvo = setorService.salvar(setor);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, setorSalvo.getCodigo()));
		return ResponseEntity.status(HttpStatus.CREATED).body(setorSalvo);
	}

	@DeleteMapping("/{codigo}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Long codigo) {
		setorRepository.delete(codigo);
	}

	@PutMapping("/{codigo}")
	public ResponseEntity<Setor> atualizar(@PathVariable Long codigo, @Valid @RequestBody Setor setor) {
		try {
			Setor setorSalvo = setorService.atualizar(codigo, setor);
			return ResponseEntity.ok(setorSalvo);
		} catch (IllegalArgumentException e) {
			return ResponseEntity.notFound().build();
		}
	}
}
