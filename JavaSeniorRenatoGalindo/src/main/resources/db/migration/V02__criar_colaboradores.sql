CREATE TABLE colaborador(

codigo BIGINT(20) PRIMARY KEY AUTO_INCREMENT,

cpf VARCHAR(50) NOT NULL ,

nome VARCHAR(50) NOT NULL ,

telefone VARCHAR(50) NOT NULL,

email VARCHAR(50)NOT NULL,

id_setores BIGINT(20) NOT NULL,

FOREIGN KEY (id_setores) REFERENCES setores(id_setores)

)ENGINE=InnoDB DEFAULT CHARSET=utf8;
